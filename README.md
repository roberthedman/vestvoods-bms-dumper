# Vestwoods BMS dumper

After realizing that the ["pow bank"](https://play.google.com/store/apps/details?id=uni.UNI0B24B11&hl=en&gl=US) app supplied by vestwoods for their batteries has been loosing features with every update, critical features such as individual cell voltages, I decided to reverse engineer the bluetooth protocol to access the data without the app.

I only own the 12V 250Ah 200A battery: [like this one](https://vestwoods.store/collections/battery/products/vestwoods-smart-12v-250ah-lifepo4-lithium-rechargeable-battery-bluetooth-monitoring-app-soc-data-4500-cycles-3-2kwh-200a-bms-low-temp-cut-off-ip65-replacement-for-rv-solar-marine-golf-cart), and so the code has only been tested with them. If your batteries contain a different number of cells than 4 you will have to adjust the get_battdata.py data indexes to reflect you cellcount.

This code is thrown together in a few evenings and not intended for other use than prototyping things.

The Plotly library is used for plotting and bleak library for the BLE interface.

There is currently a branch in development, [sql](https://gitlab.com/roberthedman/vestvoods-bms-dumper/-/tree/sql), where the [get_battdata.py](get_battdata.py) is setup to insert the logdata into an sql database instead of manually logging to file as in the example below and the plot generation is handled by cron, pushed into a web server for easy access.
The most recent code is over there, so go [there](https://gitlab.com/roberthedman/vestvoods-bms-dumper/-/tree/sql) and check it out!

## Setup / run

- Figure out your batteries serial numbers / bluetooth names. Mine are for example: DEVICE_NAME1 = "2551203230106134"
- Update the get_battdata.py file to use your serial numbers.
- run the get_battdata.py once for instantaneous information, or for continuous logging you could do something like: 
   - `while true; do echo  "" >> log9.log && date >> log9.log && python3 get_battdata.py >> log9.log; done`
- Once your data is in a file, update dataplotter.py to use your log file name, then
- run dataplotter.py 
- If you want to slightly speed up the logreading in the dataplotter you may do something like this first:
   - `cat log9.log | grep LOG | grep -E "30/11|29/11" > 29-30-nov.log ; python3 dataplotter.py`

If you run into trouble verify that the service UUIDs are correct.

## Example output
![Example graph](img/example_graph1.png "Example graph output from dataplotter.py")

## Reverse engineering process
To get here I first ran the pow_bank app normally and listened to the bluetooth traffic.
With wireshark I identified relevant bits in the payload.
After much trial and error I reached the conclusion that the following seems true:
- The app send a specific series of bytes to trigger various responses.
   - The following sequence tells the battery to dump most data of interest to me:
   - `[122, 0, 5, 0, 0, 1, 12, 229, 167] // decimal encoding`
   - I believe that "1" (6th byte) is a command selector allowing one to dump various types of data
   - I believe that "122" (1st byte) is a start of message byte.
   - The responses come back encoded in a series of bytes, see table below.

## Response decoding
- The 7th byte is the number of internal cells
- the pairwise following bytes ([8,9],[10,11],[12,13],[14,15] in the case of a 4 cell battery) are the individual cell voltages, multiplied by 1000.
   - The first bit is a signaling bit for something, don't know what yet, seems related to which cells are balancing or such. Set first bit to 0 and be happy.
- For complete list see the code in get_battdata.py
   - Many values contain an offset or multiplier that has been adjusted for already in the get_battdata.py script.


# Notes
There are more files in here which may help you find the UUIDs of services, or battery names, etc.
They are not documented nor user friendly. 
Feel free to poke around!

There is a value in dataplotter.py, N, which sets how many datapoints to skip in case you hava many to plot and the UI is slow.

## Work of others
Seems I was not first to have this problem, other people have successfully reverse engineered the entire protocol, [for example](https://github.com/justinschoeman/vestwoods_bms_protocol/tree/main).

## Possible improvements

Using something like [plotly resample](https://github.com/predict-idlab/plotly-resampler) would probably improve front end performance a lot.