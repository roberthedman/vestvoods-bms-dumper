import asyncio
import sys
from bleak import BleakClient

#HEART_RATE_ID = '00002a19-0000-1000-8000-00805f9b34fb'
#heart_rate_char = '00002a19-0000-1000-8000-00805f9b34fb'

#HEART_RATE_ID = ''
heart_rate_char = '6e400001-b5a3-f393-e0a9-e50e24dcca9e'

def heart_rate_callback(handle, data):
  print(handle, data)

async def main(address):
  async with BleakClient(address) as client:
    if (not client.is_connected):
      raise "client not connected"

    services = await client.get_services()


    client.start_notify(heart_rate_char, heart_rate_callback)
    await asyncio.sleep(60)
    await client.stop_notify(heart_rate_char)



if __name__ == "__main__":
  address = sys.argv[1]
  print('address:', address)
  asyncio.run(main(address))
