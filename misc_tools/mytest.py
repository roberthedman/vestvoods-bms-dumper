import asyncio
import sys
from bleak import BleakClient

CHAR_ID = '00002a19-0000-1000-8000-00805f9b34fb'
                #00002a19-0000-1000-8000-00805f9b34fb

async def main(address):
  async with BleakClient(address) as client:
    if (not client.is_connected):
      raise "client not connected"

    services = await client.get_services()
    
    data_bytes = await client.read_gatt_char(CHAR_ID)
    print(data_bytes)
    data = bytearray.decode(data_bytes)
    print('data:', data, '---')

if __name__ == "__main__":
  address = sys.argv[1]
  print('address:', address)
  asyncio.run(main(address))
