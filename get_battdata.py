"""
UART Service
-------------
An example showing how to write a simple program using the Nordic Semiconductor
(nRF) UART service.
"""

import asyncio
import sys
from itertools import count, takewhile
from typing import Iterator

from datetime import datetime

from bleak import BleakClient, BleakScanner
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.device import BLEDevice
from bleak.backends.scanner import AdvertisementData

#UART_SERVICE_UUID = "6e400000-b5a3-f393-e0a9-e50e24dcca9e"
#UART_RX_CHAR_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
#UART_TX_CHAR_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"

DEVICE_MAC = "2B:80:03:23:1B:92:"
#DEVICE_NAMES = ["2551203230106134", "2551203230106155", "2551203230106164", "2551203230106165", "2551203230713030", "2551203230713047", "2551203230713195", "2551203230713196"]
DEVICE_NAME1 = "2551203230106134"
DEVICE_NAME2 = "2551203230106155"
DEVICE_NAME3 = "2551203230106164"
DEVICE_NAME4 = "2551203230106165"
DEVICE_NAME5 = "2551208230713030"
DEVICE_NAME6 = "2551208230713047"
DEVICE_NAME7 = "2551208230713195"
DEVICE_NAME8 = "2551208230713197"

DEVICE_NAMES = [DEVICE_NAME1, DEVICE_NAME2, DEVICE_NAME3, DEVICE_NAME4, DEVICE_NAME5, DEVICE_NAME6, DEVICE_NAME7, DEVICE_NAME8]

UART_SERVICE_UUID = "6e400000-b5a3-f393-e0a9-e50e24dcca9e"
UART_RX_CHAR_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
UART_TX_CHAR_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"

# TIP: you can get this function and more from the ``more-itertools`` package.
def sliced(data: bytes, n: int) -> Iterator[bytes]:
    """
    Slices *data* into chunks of size *n*. The last slice may be smaller than
    *n*.
    """
    return takewhile(len, (data[i : i + n] for i in count(0, n)))


async def uart_terminal():
    """This is a simple "terminal" program that uses the Nordic Semiconductor
    (nRF) UART service. It reads from stdin and sends each line of data to the
    remote device. Any data received from the device is printed to stdout.
    """

    
    print(DEVICE_NAME)

    def match_nus_uuid(device: BLEDevice, adv: AdvertisementData):
        # This assumes that the device includes the UART service UUID in the
        # advertising data. This test may need to be adjusted depending on the
        # actual advertising data supplied by the device.

        #print(f'matching {DEVICE_NAME} to {device.name}')
        if  device.name == DEVICE_NAME:
            #print("Found match!")
            return True

        #print(f'matching {UART_SERVICE_UUID} to {device} : {adv.service_uuids}')
        #if UART_SERVICE_UUID.lower() in adv.service_uuids:
        #    return True

        return False

    device = await BleakScanner.find_device_by_filter(match_nus_uuid)

    if device is None:
        print("no matching device found, you may need to edit match_nus_uuid().")
        sys.exit(1)

    def handle_disconnect(_: BleakClient):
        print("Device was disconnected.")
        # cancelling all tasks effectively ends the program
        for task in asyncio.all_tasks():
            task.cancel()

    received_data = False

    def handle_rx(_: BleakGATTCharacteristic, data: bytearray):
        payload = bytearray()
        start_of_message_found = False
        for v in data:
            #print(f"  {v:02x}")
            if v == 0x7a:
                start_of_message_found = True
            if start_of_message_found:
                payload.append(v)
        i = 0
        for v in payload:
            #print(f"{i}: {v:02x}")
            i = i+1
        decode_payload(payload)

        nonlocal received_data
        received_data = True



    def decode_payload(data: bytearray):
        if len(data) < 58:
            return

        # Get the 7th byte as num_cells
        num_cells = data[7]
        

        print("Cell voltages:")
        cellvs = []

        if num_cells != 4: # Data is corrupt.
            print(f"not 4 cells in data ({num_cells}), skipping...")
            print(data)
            return

        index = 8
        for _ in range(num_cells):
            # Extract a 4-byte value and convert it to a single value
            bytes1 = data[index]
            bytes2 = data[index+1]
            bytes1 = bytes1 & 0b01111111
            value_bytes = [bytes1, bytes2] #data[index:index+2]
            single_value = int.from_bytes(value_bytes, byteorder='big') / 1000.0
            cellvs.append(single_value)
            # Print the single value 
            print(f"{data[index]:02x}{data[index+1]:02x} : {data[index]:08b} {data[index+1]:08b} = {single_value}")
            # Increment the index for the next iteration
            index += 2

        total_current = int.from_bytes(data[22:24], byteorder='big') / 100.0 -300
        print(f"Current: {total_current}A")

        soc = int.from_bytes(data[24:26], byteorder='big') / 100
        print(f"soc: {soc}%")
        soh = int.from_bytes(data[26:28], byteorder='big') / 100
        print(f"soh: {soh}%")

        surplus_capacity = int.from_bytes([data[30]&0b01111111, data[31]], byteorder='big') / 100
        print(f"surplus_capacity: {surplus_capacity}Ah")
        index = 30
        print(f"{data[index]:02x}{data[index+1]:02x} : {data[index]:08b} {data[index+1]:08b}")
        
        actual_capacity = int.from_bytes([data[28]&0b01111111, data[29]], byteorder='big') / 100
        print(f"actual_capacity: {actual_capacity}Ah")
        index = 28
        print(f"{data[index]:02x}{data[index+1]:02x} : {data[index]:08b} {data[index+1]:08b}")
        
        nominal_capacity = int.from_bytes(data[32:34], byteorder='big') / 100
        print(f"nominal_capacity: {nominal_capacity}Ah")
        index = 32
        print(f"{data[index]:02x}{data[index+1]:02x} : {data[index]:08b} {data[index+1]:08b}")

        voltage = int.from_bytes(data[55:57], byteorder='big') / 100
        print(f"voltage: {voltage}V")

        cycles = int.from_bytes(data[53:55], byteorder='big') 
        print(f"cycles: {cycles}")

        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        index=30
        print(f"LOG: {DEVICE_NAME} {dt_string} {cellvs[0]} {cellvs[1]} {cellvs[2]} {cellvs[3]} {voltage} {total_current} {soc} {soh} {surplus_capacity} {data[index]:08b} {data[index+1]:08b} {actual_capacity} {nominal_capacity} {cycles}")


        


    async with BleakClient(device, disconnected_callback=handle_disconnect) as client:
        await client.start_notify(UART_TX_CHAR_UUID, handle_rx)

        loop = asyncio.get_running_loop()
        nus = client.services.get_service(UART_SERVICE_UUID)
        rx_char = nus.get_characteristic(UART_RX_CHAR_UUID)

        #trigger_message = [122, 0, 5, 0, 0, 1, 12, 229, 167] // decimal
        trigger_message = bytearray([0x7a, 0x00, 0x05, 0x00, 0x00, 0x01, 0x0c, 0xe5, 0xa7])
        #print(f"Sending: {trigger_message}")
        for s in sliced(trigger_message, rx_char.max_write_without_response_size):
                await client.write_gatt_char(rx_char, s)

        while not received_data:
            #print(received_data)
            await asyncio.sleep(0.1)




if __name__ == "__main__":
    #loop = asyncio.get_event_loop()
    for batt in DEVICE_NAMES:
        DEVICE_NAME = batt
        print()
        try:
            asyncio.run(uart_terminal())
        except asyncio.CancelledError:
            # task is cancelled on disconnect, so we ignore this error
            pass
