import plotly.express as px
from plotly.subplots import make_subplots
import pandas as pd

import cProfile
import re



#from plotly_resampler import FigureResampler

def plotbatt():
    # Open the file in read mode
    #file_path = "log9.log"
    file_path = "21nov.log"
    battery_data = {}

    print("Reading file...")
    try:
        with open(file_path, "r") as file:
            # Read the file line by line
            for line in file:
                # Check if the line starts with "LOG:"
                if line.startswith("LOG:"):
                    # Split the log line by spaces to separate fields
                    parts = line.strip().split()
                    #prefix     name          day       time   cell1  cell2 cell3 cell4 V    Current           soc  soh   surplus   binary-surplus  actual nominal cycles
                    # 0 	1 				  2			3		4		5	6	  7		8	 9 				   10   11    12      13       14       15    16   17
                    #LOG: 2551203230106165 01/11/2023 12:00:16 3.383 3.384 3.37 3.385 13.52 53.89999999999998 27.1 100.0 169.11 01000010 00001111 295.07 250.0 6
                    if len(parts) >= 11:
                        battery_name = parts[1] 
                        date = parts[2]
                        time = parts[3]
                        full_datetime = pd.to_datetime(f"{date} {time}", dayfirst=True)
                        voltage = float(parts[8])
                        current = float(parts[9])
                        soc = float(parts[10])
                        

                    if battery_name not in battery_data:
                        battery_data[battery_name] = {'Datetime': [], 'Voltage': [], 'Current': [], 'soc': []}

                    battery_data[battery_name]['Datetime'].append(full_datetime)
                    battery_data[battery_name]['Voltage'].append(voltage)
                    battery_data[battery_name]['Current'].append(current)
                    battery_data[battery_name]['soc'].append(soc)

    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
    except Exception as e:
        print(f"An error occurred: {e}")

    print("File read! Calculating system voltage...")

    #Create sum of voltages for system voltage. Do not care about differing timestamps.
    total_v = []
    timestamps = []
    for batt_name, data in battery_data.items():
    	if len(timestamps) < len(data['Datetime']): # 
    		timestamps = data['Datetime'] 
    	for i, v in enumerate(data['Voltage']):
    		#print(f"{batt_name} {i}")
    		if i>=len(total_v):
    			total_v.append(data['Voltage'][i])
    		else:
    			total_v[i] = total_v[i] + data['Voltage'][i]

    print("Done! Plotting...")



    fig = make_subplots(rows=3, cols=1, shared_xaxes=True,)
    colors = ['blue','red','magenta','green', 'orange', 'black','pink', 'cyan']
    markersize = 2
    modetext = "markers+lines"
    linewidth = 0.1

    #fig.add_scatter(x=timestamps, y=total_v, name=f"System Voltage", row=2, col=1, mode=modetext, marker_size=markersize, line_width=linewidth) #, showlegend=False,) # mode='lines'
    i = 0
    # Plot every Nth datapoint. If you have many datapoints you may want to increase this value to improve frontend responsiveness.
    N = 1 #25
    for battery_name, data in battery_data.items():
        df = pd.DataFrame(data)


        fig.add_scatter(x=df['Datetime'][::N], y=df['Voltage'][::N], name=f"{battery_name[-3:]}", row=1, col=1, mode=modetext, line_color=colors[i], marker_size=markersize, line_width=linewidth)
        fig.add_scatter(x=df['Datetime'][::N], y=df['soc'][::N], name=f"{battery_name[-3:]}", row=2, col=1, showlegend=False, mode=modetext, line_color=colors[i], marker_size=markersize, line_width=linewidth)
        fig.add_scatter(x=df['Datetime'][::N], y=df['Current'][::N], name=f"{battery_name[-3:]}", row=3, col=1, showlegend=False, mode=modetext, line_color=colors[i], marker_size=markersize, line_width=linewidth) #, line_smoothing=1.3) #mode=lines
        i=i+1

    # Customize the layout
    fig.update_xaxes(title_text="Battery graphs")
    #fig.update_yaxes(title_text="System Voltage", row=1,col=1)
    fig.update_yaxes(title_text="Battery Voltage", row=1,col=1, range=[10,15])
    fig.update_yaxes(title_text="System Voltage / State of Charge (soc)", row=2,col=1, range=[0,100])
    fig.update_yaxes(title_text="Curent", row=3,col=1)
    fig.update_layout(legend_title_text="Battery Name")

    # Show the plot
    fig.show()




cProfile.run('plotbatt()')


